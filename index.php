<?php
// Requires
require "bootstrap.php";
require "config/config.php";
// Uses
use User\Models\User;
use User\Models\Message;
use User\Middleware\Logging;
use User\Middleware\Cors;
use User\Middleware\Auth;
use \Firebase\JWT\JWT;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

$app = new \Slim\App(['settings' => ['displayErrorDetails' => true]]);
$app ->add(new logging());
$app ->add(new Auth());


// Directives
$container = $app->getContainer();
$container['upload_directory'] = __DIR__ . '/uploads';
// ----------------------------------------------- TESTING ----------------------------------------------------------------------//
$app->get('/test', function($request, $response,$args){    
    echo $hash = password_hash("asdasd",PASSWORD_DEFAULT);
    echo "@@@@@@ <br /><br />";
    if(password_verify("asdasd",$hash)){
        echo true;
    }else{
        echo false;
    }
    $key = "example_key";
    $initial = new DateTime();
    $expire  = new DateTime();
    $expire->add(new DateInterval('PT' . 30 . 'M'));

    $token = array(
        "initial" => $initial->getTimestamp(),
        "expire"  => $expire->getTimestamp()
    );
    $jwt = JWT::encode($token, $key);    
    //$decoded = JWT::decode($jwt, $key, array('HS256'));
    
    //return $response->withStatus(200)->withJson($jwt)->withHeader('Access-Control-Allow-Origin', '*');
});
// ----------------------------------------------- TESTING ----------------------------------------------------------------------//
//-------------------------------   Messeages  ---------------------------------------------------------------------------------------
$app->get('/messages', function($request, $response,$args){
    sleep(1);
    $_message = new Message();    
    $messages = $_message->all();
    $payload = [];
    foreach($messages as $msg){
        $_user = User::find($msg->user_id);

        $payload[$msg->id] = [
            'title'=>$msg->title,
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'image'=> 'http://matanco.myweb.jce.ac.il/31023/project/slim/uploads/' . basename($_user->image),
            'created_at'=>$msg->created_at
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->get('/messages_by_user/{user_id}', function($request, $response,$args){    
    sleep(1);
    $messages = Message::where('user_id', '=', $args['user_id'])->get();
    $_user = User::find($args['user_id']);
    $payload = [];
    foreach($messages as $msg){        

        $payload[$msg->id] = [
            'title'=>$msg->title,
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'image'=> 'http://matanco.myweb.jce.ac.il/31023/project/slim/uploads/' . basename($_user->image),
            'created_at'=>$msg->created_at
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->get('/messages/{id}', function($request, $response,$args){
    sleep(1);
    $_id = $args['id'];
    $message = Message::find($_id);
    return $response->withStatus(200)->withJson($message);
});

$app->post('/messages', function($request, $response,$args){
    $body    = $request->getParsedBodyParam('body','');
    $title   = $request->getParsedBodyParam('title','');
    $user_id = $request->getParsedBodyParam('user_id','');
    $_message = new Message();
    $_message->title = $title;
    $_message->body = $body;
    $_message->user_id = $user_id;
    $_message->save();
    if($_message->id){
        $payload = ['message_id'=>$_message->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400);
    }
});

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $message = Message::find($args['message_id']); //find the message in database
    $message->delete(); //delete the message
    if($message->exists){ //check if the message already exit
        return $response->withStatus(400); //exist -> false error
    } else {
        return $response->withStatus(200); //message delete succesfully
    }
});

$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message',''); //get the parameters from the post
    $_message = Message::find($args['message_id']); // search the message with the id from the url
    $_message->body = $message; //delete the old param, and insert the new param
    if($_message->save()){ //the update succeed
        $payload = ['message_id'=>$_message->id, "result"=>"The message has been update succesfully"]; //useful to the json that return to the user about the update status
        return $response->withStatus(201)->withJson($payload); //succes
    } else{
        return $response->withStatus(400); //error
    }
});

$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody(); //convert the JSON to array of
    Message::insert($payload); //insert to the DB all what come from the JSON
    return $response->withStatus(201)->withJson($payload);
});
//-------------------------------   Messeages  ---------------------------------------------------------------------------------------
//-------------------------------   Users  -------------------------------------------------------------------------------------------
$app->get('/users', function($request, $response,$args){ 
    $_user = new User();
    $users = $_user->all();
    $payload=[];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=> $usr->id,
            'name'=> $usr->name,
            'phone'=> $usr->phone,
            'image'=> $usr->image,
        ];
    }
    return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});

$app->get('/users/{user_id}', function($request, $response,$args){   
    $_user = User::find($args['user_id']);

    $payload = [
            'id'=> $_user->id,
            'name'=> $_user->name,
            'phone'=> $_user->phone,
            'email'=> $_user->email,
            'gender'=> $_user->gender,
            'dob'=> $_user->dob,
            'image'=> 'http://matanco.myweb.jce.ac.il/31023/project/slim/uploads/' . basename($_user->image),
            'created_at'=> $_user->created_at,
            'updated_at' => $_user->updated_at
    ];

    if($_user->id){
      return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }else{
      return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }  
    
});

$app->post('/users/create', function($request, $response, $args){  
    $directory = $this->get('upload_directory');
    $uploadedFiles = $request->getUploadedFiles();
    
    $name              = $request->getParsedBodyParam('name','');    
    $phone             = $request->getParsedBodyParam('phone','');
    $dob               = $request->getParsedBodyParam('dob','');
    $gender            = $request->getParsedBodyParam('gender','');
    $email             = $request->getParsedBodyParam('email','');
    $password          = password_hash($request->getParsedBodyParam('password',''), PASSWORD_DEFAULT);     
    $uploadedFile      = $uploadedFiles['image'];    

    $_user             = new User();
    $_user->name       = $name;
    $_user->phone      = $phone;
    $_user->password   = $password;
    $_user->dob        = $dob;
    $_user->email      = $email;
    $_user->gender     = $gender;

    if($uploadedFile->getError() === UPLOAD_ERR_OK){
        $filename          = moveUploadedFile($directory, $uploadedFile);
        $_user->image      = $directory . DIRECTORY_SEPARATOR . $filename;
    }        
    $_user->save();

    if($_user->id){        
        $payload = ['user_id'=>$_user->id, 'password'=> $request->getParsedBodyParam('password','')];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});

$app->delete('/users/delete/{user_id}', function($request, $response,$args){
    $_user = User::find($args['user_id']); 
    $_user->delete();
    if($_user->exist){
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(200)->withHeader('Access-Control-Allow-Origin', '*');
        
    }
});

$app->put('/users/{user_id}', function($request, $response,$args){
    $directory = $this->get('upload_directory');

    $name              = $request->getParsedBodyParam('name','');
    $phone             = $request->getParsedBodyParam('phone','');
    $uploadedFile      = $uploadedFiles['image'];

    $_user = User::find($args['user_id']);
    $_user->name   = $name;
    $_user->phone  = $phone;
    if($uploadedFile->getError() === UPLOAD_ERR_OK){
        $filename          = moveUploadedFile($directory, $uploadedFile);
        $_user->image      = $directory . DIRECTORY_SEPARATOR . $filename;
    }      
    if( $_user->save()){
        $payload = ['user_id'=>$_user->id,"result"=>"The user was updated successfuly"];
        return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});

$app->post('/users/bulk', function($request, $response,$args){
    $payload =$request->getParsedBody();
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }

);

$app->post('/login', function($request, $response,$args){
    $username  = $request->getParsedBodyParam('username','');
    $password  = $request->getParsedBodyParam('password','');    
    $_user     = User::where('name', '=', $username)->get();

    if($_user[0]->id){
        if(password_verify($password, $_user[0]->password)){
            $token = generateJWT($secret);
            $_user[0]->token = $token;
            $_user[0]->save();
            $payload = [ 'token' => $token, 'user_id' => $_user[0]->id ];

            return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
        }else{
            $payload = ['status'=>'failed', 'password'=> $password, 'hash' => $_user[0]->password, 'v' => password_verify($password, $_user[0]->password)];
            return $response->withStatus(400)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
        }        
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});


$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization, Token')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');            
});

/**
 * Moves the uploaded file to the upload directory and assigns it a unique name
 * to avoid overwriting an existing uploaded file.
 *
 * @param string $directory directory to which the file is moved
 * @param UploadedFile $uploaded file uploaded file to move
 * @return string filename of moved file
 */
function moveUploadedFile($directory, UploadedFile $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}

function generateJWT($secret){
    $initial = new DateTime();
    $expire  = new DateTime();
    $expire->add(new DateInterval('PT' . 30 . 'M'));

    $token = array(
        "initial" => $initial->getTimestamp(),
        "expire"  => $expire->getTimestamp()
    );
    $jwt = JWT::encode($token, $secret);

    return $jwt;  
}


$app->run();

